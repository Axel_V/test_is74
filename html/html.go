package html

import (
	"io"

	"golang.org/x/net/html"
)

func ExtractTitle(r io.Reader) string {
	doc, err := html.Parse(r)
	if err != nil {
		return ""
	}
	node := findElementNode(doc, "title")
	if node == nil {
		return ""
	}

	node = findTextNode(node)
	if node == nil {
		return ""
	}
	return node.Data
}

func findElementNode(doc *html.Node, tagName string) *html.Node {
	var desired *html.Node
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == tagName {
			desired = n
			return
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	return desired
}

func findTextNode(node *html.Node) *html.Node {
	var desired *html.Node
	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.TextNode {
			desired = n
			return
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(node)
	return desired
}
