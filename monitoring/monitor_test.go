package monitoring

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"
	"testing"
)

var testCases = []struct {
	Result *Result
	fn     buildLinkFunc
}{
	{
		Result: &Result{IP: net.IP{127, 0, 0, 1}, Code: 200, Title: "", Location: ""},
		fn:     buildTestLinkOK,
	},
	{
		Result: &Result{IP: net.IP{127, 0, 0, 1}, Code: 200, Title: "Test Title", Location: ""},
		fn:     buildTestLinkHTML,
	},
	{
		Result: &Result{IP: net.IP{127, 0, 0, 1}, Code: 301, Title: "", Location: "http://testaddr/redirect"},
		fn:     buildTestLinkRedirect,
	},
	{
		Result: &Result{IP: nil, Code: 500, Title: "", Location: ""},
		fn:     buildTestLinkFail,
	},
}

const port = 9091

func TestMain(m *testing.M) {
	go runTestServer(port)
	m.Run()
}

func TestMonitorWithPool_Monitor(t *testing.T) {
	worker := newMonitorWithPool()
	for _, testCase := range testCases {
		link := testCase.fn(port)
		got, err := worker.Monitor(link)
		if err != nil {
			t.Fatal(err)
		}
		compareResults(t, got, testCase.Result)
	}
}

func TestMonitorWithStore_Monitor(t *testing.T) {
	worker := newMonitorWithStore()
	for _, testCase := range testCases {
		link := testCase.fn(port)
		got, err := worker.Monitor(link)
		if err != nil {
			t.Fatal(err)
		}
		compareResults(t, got, testCase.Result)
	}
}

func runTestServer(port int) {
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
	mux.HandleFunc("/html", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte(testHTML))
	})
	mux.HandleFunc("/redirect", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Location", "http://testaddr/redirect")
		w.WriteHeader(http.StatusMovedPermanently)
	})
	err := http.ListenAndServe(":"+strconv.Itoa(port), mux)
	if err != nil {
		log.Fatal(err)
	}
}

func compareResults(t *testing.T, r1, r2 *Result) {
	if r1.IP != nil && r2.IP != nil {
		if !r1.IP.IsLoopback() || !r2.IP.IsLoopback() {
			t.Fatalf("result ips are not loopback %+v != %+v", r1, r2)
		}
	} else {
		if (r1.IP == nil) != (r2.IP == nil) {
			t.Fatalf("result ips are not equal %+v != %+v", r1, r2)
		}
	}
	if r1.Code != r2.Code {
		t.Fatalf("result codes not equal %+v != %+v", r1, r2)
	}
	if r1.Title != r2.Title {
		t.Fatalf("result titles not equal %+v != %+v", r1, r2)
	}
	if r1.Location != r2.Location {
		t.Fatalf("result locations not equal %+v != %+v", r1, r2)
	}
}

type buildLinkFunc func(port int) Link

func buildTestLinkOK(port int) Link {
	return Link(fmt.Sprintf("http://localhost:%d", port))
}

func buildTestLinkHTML(port int) Link {
	return Link(fmt.Sprintf("http://localhost:%d/html", port))
}

func buildTestLinkRedirect(port int) Link {
	return Link(fmt.Sprintf("http://localhost:%d/redirect", port))
}

func buildTestLinkFail(port int) Link {
	return Link(fmt.Sprintf("http://localhost:%d/fail", port+1))
}

const testHTML = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test Title</title>
</head>
<body>
</body>
</html>`
