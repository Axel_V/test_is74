package monitoring

import (
	"context"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"sync/atomic"

	"service-monitor/html"
	"service-monitor/transport"
)

// Monitorer represents monitoring service
// There are two available ways: monitor with transport pool and monitor with ip internal storage.
// Transport pool provides executing each request on individual *http.Client with ip extracting.
// Monitor with ip internal storage provides usual way executing requests with extracting ip
// from storage after request has done.
// You can control the behaviour with option Config.UsePool
type Monitorer interface {
	Monitor(link Link) (*Result, error)
}

func NewMonitor(config *Config) Monitorer {
	if config.UsePool {
		return newMonitorWithPool()
	}
	return newMonitorWithStore()
}

type Config struct {
	UsePool bool
	// could be expanded with transport timeouts
}

type Link string

func (l Link) ToURL() (*url.URL, error) {
	u, err := url.Parse(string(l))
	if err != nil {
		return nil, err
	}
	if u.Scheme == "" {
		u.Scheme = "https"
	}
	return u, nil
}

type Result struct {
	Link     Link
	IP       net.IP
	Code     int
	Title    string
	Location string
}

func (r *Result) String() string {
	ip := string(r.Link)
	if r.IP != nil {
		ip = r.IP.String()
	}
	if r.Location != "" {
		ip = strings.TrimPrefix(r.Location, "https://")
		ip = strings.TrimPrefix(ip, "http://")
	}
	title := r.Title
	if title == "" {
		title = "-"
	}
	return fmt.Sprintf("%s | %s | %d | %s", r.Link, ip, r.Code, title)
}

type monitorWithPool struct {
	transport *transport.Pool
}

func newMonitorWithPool() *monitorWithPool {
	return &monitorWithPool{transport: transport.NewPool()}
}

func (m *monitorWithPool) Monitor(link Link) (*Result, error) {
	reqURL, err := link.ToURL()
	if err != nil {
		return nil, err
	}

	var remoteIP net.IP
	client := m.transport.AcquireClient(transport.DialRemoteIPWrapper(func(ip net.IP) {
		remoteIP = ip
	}))
	defer m.transport.ReleaseClient(client)

	resp, err := client.Get(reqURL.String())
	if err != nil {
		if strings.Contains(err.Error(), transport.ErrIOTimeoutText) ||
			strings.Contains(err.Error(), transport.ErrConnectionRefusedText) {
			return &Result{Link: link, IP: remoteIP, Code: 500}, nil
		}
		return nil, err
	}
	defer func() {
		// for closing tcp connection correctly
		_, _ = io.ReadAll(resp.Body)
		_ = resp.Body.Close()
	}()

	var title, location string
	switch {
	case resp.StatusCode == http.StatusOK:
		title = html.ExtractTitle(resp.Body)
	case resp.StatusCode >= 300 && resp.StatusCode < 400:
		location = resp.Header.Get("Location")
	}

	return &Result{
		Link:     link,
		IP:       remoteIP,
		Code:     resp.StatusCode,
		Title:    title,
		Location: location,
	}, nil
}

const ctxKeyRequestID = "request_id"

type monitorWithStore struct {
	client    *http.Client
	requestID *int64 // atomic usage only

	muIPs sync.Mutex // sync.Map would be more effective on system with high amount of cores (32+)
	ips   map[int64]net.IP
}

func newMonitorWithStore() *monitorWithStore {
	monitor := &monitorWithStore{
		requestID: new(int64),
		ips:       map[int64]net.IP{},
	}
	client := transport.NewClientWithDialWrapper(monitor.dialWrapper())
	monitor.client = client
	return monitor
}

func (m *monitorWithStore) Monitor(link Link) (*Result, error) {
	reqURL, err := link.ToURL()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodGet, reqURL.String(), http.NoBody)
	if err != nil {
		return nil, err
	}
	requestID := atomic.AddInt64(m.requestID, 1)
	ctx := context.WithValue(context.Background(), ctxKeyRequestID, requestID)
	req = req.WithContext(ctx)

	var remoteIP net.IP
	resp, err := m.client.Do(req)
	if err != nil {
		if strings.Contains(err.Error(), transport.ErrIOTimeoutText) ||
			strings.Contains(err.Error(), transport.ErrConnectionRefusedText) {
			return &Result{Link: link, IP: remoteIP, Code: 500}, nil
		}
		return nil, err
	}
	defer func() {
		// for closing tcp connection correctly
		_, _ = io.ReadAll(resp.Body)
		_ = resp.Body.Close()
	}()
	remoteIP = m.ipByRequestID(requestID)

	var title, location string
	switch {
	case resp.StatusCode == http.StatusOK:
		title = html.ExtractTitle(resp.Body)
	case resp.StatusCode >= 300 && resp.StatusCode < 400:
		location = resp.Header.Get("Location")
	}

	return &Result{
		Link:     link,
		IP:       remoteIP,
		Code:     resp.StatusCode,
		Title:    title,
		Location: location,
	}, nil
}

func (m *monitorWithStore) putIPByRequestID(requestID int64, ip net.IP) {
	m.muIPs.Lock()
	m.ips[requestID] = ip
	m.muIPs.Unlock()
}

func (m *monitorWithStore) ipByRequestID(requestID int64) net.IP {
	var ip net.IP

	m.muIPs.Lock()
	ip = m.ips[requestID]
	delete(m.ips, requestID)
	m.muIPs.Unlock()

	return ip
}

func (m *monitorWithStore) dialWrapper() transport.DialContextWrapper {
	return func(dialContext transport.DialContext) transport.DialContext {
		return func(ctx context.Context, network, addr string) (net.Conn, error) {
			requestID, _ := ctx.Value(ctxKeyRequestID).(int64)
			conn, dialErr := dialContext(ctx, network, addr)
			if requestID != 0 && conn != nil {
				host, _, _ := net.SplitHostPort(conn.RemoteAddr().String())
				m.putIPByRequestID(requestID, net.ParseIP(host))
			}
			return conn, dialErr
		}
	}
}
