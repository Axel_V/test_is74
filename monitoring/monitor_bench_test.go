package monitoring

import "testing"

func BenchmarkMonitorWithPool_Monitor(b *testing.B) {
	link := buildTestLinkOK(port)
	worker := newMonitorWithPool()

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = worker.Monitor(link)
	}
}

func BenchmarkMonitorWithStore_Monitor(b *testing.B) {
	link := buildTestLinkOK(port)
	worker := newMonitorWithPool()

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = worker.Monitor(link)
	}
}
