package monitoring

import (
	"context"
	"log"
)

type Worker struct {
	monitor Monitorer
}

func NewWorker(monitor Monitorer) *Worker {
	return &Worker{
		monitor: monitor,
	}
}

func (w *Worker) Run(ctx context.Context, links <-chan string, goroutinesCount int) <-chan *Result {
	results := make(chan *Result)

	for i := 0; i < goroutinesCount; i++ {
		go func(links <-chan string, results chan<- *Result) {
			for {
				select {
				case <-ctx.Done():
					return
				case link := <-links:
					result, err := w.monitor.Monitor(Link(link))
					if err != nil {
						log.Printf("monitoring error: %s", err)
						continue
					}
					results <- result
				}
			}
		}(links, results)
	}

	return results
}
