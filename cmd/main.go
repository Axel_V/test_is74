package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"service-monitor/io"
	"service-monitor/monitoring"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	var (
		input, output   string
		goroutinesCount int
	)

	flag.StringVar(&input, "i", "input.txt", "Input file (default input.txt)")
	flag.StringVar(&output, "o", "output.txt", "Output file (default output.txt)")
	flag.IntVar(&goroutinesCount, "n", 10000, "Goroutines count (default 10000)")

	updatesChan, err := io.WatchInputFile(ctx, input)
	if err != nil {
		log.Fatal(err)
	}

	monitor := monitoring.NewMonitor(&monitoring.Config{UsePool: false})
	worker := monitoring.NewWorker(monitor)
	results := worker.Run(ctx, updatesChan, goroutinesCount)

	outFile, err := io.NewOutputFile(output)
	if err != nil {
		log.Fatal(err)
	}

	go func() {
		for result := range results {
			_, err := outFile.WriteString(result.String() + "\n")
			if err != nil {
				log.Printf("result write error: %s\n", err)
			}
		}
	}()

	log.Printf("monitoring worker started")
	select {
	case <-ctx.Done():
	case <-signalCh:
		cancel()
	}

	time.Sleep(time.Second)
}
