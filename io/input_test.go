package io

import (
	"context"
	"os"
	"testing"
)

func TestWatchInputFile(t *testing.T) {
	testCases := struct {
		preload []string
		updates []string
	}{
		preload: []string{"1234", "abcd"},
		updates: []string{"5678", "efgh", "test", "string"},
	}

	filename := os.TempDir() + "input_test.txt"
	file, err := os.Create(filename)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		_ = file.Close()
		_ = os.Remove(filename)
	}()

	// write preload
	for _, s := range testCases.preload {
		_, _ = file.WriteString(s + "\n")
	}

	updates, err := WatchInputFile(context.Background(), filename)
	if err != nil {
		t.Fatal(err)
	}

	// check preload data
	for _, expected := range testCases.preload {
		if got := <-updates; got != expected {
			t.Fatalf("preload data not equal \ngot: %s \nexp: %s", got, expected)
		}
	}

	// write updates
	go func() {
		for _, s := range testCases.updates {
			_, _ = file.WriteString(s + "\n")
		}
	}()

	// check updates data
	for _, expected := range testCases.updates {
		if got := <-updates; got != expected {
			t.Fatalf("updates data not equal \ngot: %s \nexp: %s", got, expected)
		}
	}
}
