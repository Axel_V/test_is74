package io

import (
	"bufio"
	"context"
	"io"
	"log"
	"os"

	"github.com/fsnotify/fsnotify"
)

func WatchInputFile(ctx context.Context, filename string) (_ <-chan string, err error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	updatesChan := make(chan string, 100)

	var (
		scanner *bufio.Scanner
		filePos int64
	)
	scan := func() {
		_, _ = file.Seek(filePos, io.SeekStart)
		scanner = bufio.NewScanner(file)
		scanner.Split(bufio.ScanLines)
		for scanner.Scan() {
			updatesChan <- scanner.Text()
		}
		filePos, _ = file.Seek(0, io.SeekCurrent)
	}

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		_ = file.Close()
		return nil, err
	}

	go func() {
		defer func() {
			_ = file.Close()
			_ = watcher.Close()
			close(updatesChan)
		}()
		scan()
		for {
			select {
			case <-ctx.Done():
				return
			case event := <-watcher.Events:
				if event.Op != fsnotify.Write {
					continue
				}
				scan()
			case err := <-watcher.Errors:
				log.Printf("file watcher `%s` error: %s", filename, err)
			}
		}
	}()

	if err = watcher.Add(filename); err != nil {
		return nil, err
	}

	return updatesChan, nil
}
