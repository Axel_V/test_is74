package io

import (
	"os"
	"syscall"
)

type OutputFile struct {
	file     *os.File
	filename string
}

func NewOutputFile(filename string) (*OutputFile, error) {
	of := &OutputFile{filename: filename}
	err := of.recreate()
	if err != nil {
		return nil, err
	}
	return of, nil
}

func (of *OutputFile) WriteString(s string) (n int, err error) {
	exists, err := of.isFileExists()
	if err != nil {
		return 0, err
	}
	if !exists {
		err = of.recreate()
		if err != nil {
			return 0, err
		}
	}
	return of.file.WriteString(s)
}

func (of *OutputFile) Close() error {
	return of.file.Close()
}

func (of *OutputFile) recreate() (err error) {
	of.file, err = os.OpenFile(of.filename, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	return
}

func (of *OutputFile) isFileExists() (bool, error) {
	stat, err := of.file.Stat()
	if err != nil {
		return false, err
	}
	if sys := stat.Sys(); sys != nil {
		if stat, ok := sys.(*syscall.Stat_t); ok {
			if stat.Nlink == 0 {
				return false, nil
			}
		}
	}
	return true, nil
}
