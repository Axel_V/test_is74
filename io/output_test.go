package io

import (
	"os"
	"testing"
)

func TestOutputFile_WriteString(t *testing.T) {
	const testString = "testString\n"

	filename := "output_test.txt"
	defer func() {
		_ = os.Remove(filename)
	}()

	output, err := NewOutputFile(filename)
	checkError(t, err)

	_, err = output.WriteString(testString)
	checkError(t, err)

	// take the file
	err = os.Remove(filename)
	checkError(t, err)

	exists, err := output.isFileExists()
	checkError(t, err)
	if exists {
		t.Fatal("wrong file existing detection")
	}

	_, err = output.WriteString(testString)
	checkError(t, err)

	b, err := os.ReadFile(filename)
	checkError(t, err)
	if string(b) != testString {
		t.Fatal("wrong file writing")
	}
}

func checkError(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err)
	}
}

func BenchmarkOutputFile_isFileExists(b *testing.B) {
	filename := os.TempDir() + "output_bench.txt"
	output, err := NewOutputFile(filename)
	if err != nil {
		b.Fatal(err)
	}
	defer func() {
		_ = output.Close()
		_ = os.Remove(filename)
	}()

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err = output.isFileExists()
		if err != nil {
			b.Fatal(err)
		}
	}
}
