module service-monitor

go 1.17

require (
	github.com/fsnotify/fsnotify v1.5.1
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd
)

require golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
