package transport

import (
	"net"
	"net/http"
	"testing"
)

func TestPool(t *testing.T) {
	callbackCalled := false

	pool := NewPool()
	client := pool.AcquireClient(DialRemoteIPWrapper(func(remoteIP net.IP) {
		callbackCalled = true
		if !remoteIP.IsLoopback() {
			t.Fatal("wrong remoteIP")
		}
	}))

	req, err := buildTestRequest(port)
	if err != nil {
		t.Fatal(err)
	}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != http.StatusOK {
		t.Fatalf("wrong response code %d", resp.StatusCode)
	}
	if !callbackCalled {
		t.Fatalf("callback had not been called")
	}
}
