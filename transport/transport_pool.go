package transport

import (
	"net/http"
	"sync"
)

type Pool struct {
	pool sync.Pool
}

func (p *Pool) AcquireClient(wrapper DialContextWrapper) *http.Client {
	client, ok := p.pool.Get().(*http.Client)
	if !ok {
		client = newHTTPClient()
	}
	client.Transport = newHTTPTransport(wrapper)
	return client
}

func (p *Pool) ReleaseClient(client *http.Client) {
	p.pool.Put(client)
}

func NewPool() *Pool {
	return &Pool{
		pool: sync.Pool{
			New: func() interface{} {
				return newHTTPClient
			},
		},
	}
}
