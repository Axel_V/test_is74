package transport

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"strconv"
	"testing"
)

const port = 9090

func TestMain(m *testing.M) {
	go runTestServer(port)
	m.Run()
}

func TestNewClientWithDialWrapper(t *testing.T) {
	callbackCalled := false
	client := NewClientWithDialWrapper(DialRemoteIPWrapper(func(remoteIP net.IP) {
		callbackCalled = true
		if !remoteIP.IsLoopback() {
			t.Fatal("wrong remoteIP")
		}
	}))

	req, err := buildTestRequest(port)
	if err != nil {
		t.Fatal(err)
	}
	resp, err := client.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != http.StatusOK {
		t.Fatalf("wrong response code %d", resp.StatusCode)
	}
	if !callbackCalled {
		t.Fatalf("callback had not been called")
	}
}

func runTestServer(port int) {
	err := http.ListenAndServe(":"+strconv.Itoa(port), http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	}))
	if err != nil {
		log.Fatal(err)
	}
}

func buildTestRequest(port int) (*http.Request, error) {
	reqURL := fmt.Sprintf("http://localhost:%d", port)
	return http.NewRequest(http.MethodGet, reqURL, http.NoBody)
}
