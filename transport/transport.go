package transport

import (
	"context"
	"net"
	"net/http"
	"time"
)

const (
	ErrIOTimeoutText         = "i/o timeout"
	ErrConnectionRefusedText = "connection refused"
)

func NewClientWithDialWrapper(wrapper DialContextWrapper) *http.Client {
	client := newHTTPClient()
	client.Transport = newHTTPTransport(wrapper)
	return client
}

func DialRemoteIPWrapper(callback func(remoteIP net.IP)) DialContextWrapper {
	return func(dialContext DialContext) DialContext {
		return func(ctx context.Context, network, addr string) (net.Conn, error) {
			conn, dialErr := dialContext(ctx, network, addr)
			if conn != nil {
				host, _, _ := net.SplitHostPort(conn.RemoteAddr().String())
				callback(net.ParseIP(host))
			}
			return conn, dialErr
		}
	}
}

func newHTTPClient() *http.Client {
	return &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
}

type DialContext func(ctx context.Context, network, addr string) (net.Conn, error)

type DialContextWrapper func(dialContext DialContext) DialContext

func newHTTPTransport(dialContextWrapper DialContextWrapper) *http.Transport {
	dialContext := newDialContext()
	if dialContextWrapper != nil {
		dialContext = dialContextWrapper(dialContext)
	}
	return &http.Transport{
		DialContext:           dialContext,
		DisableKeepAlives:     true,
		ForceAttemptHTTP2:     true,
		TLSHandshakeTimeout:   5 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}
}

func newDialContext() DialContext {
	return (&net.Dialer{
		Timeout: 5 * time.Second,
	}).DialContext
}
